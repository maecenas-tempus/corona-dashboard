### Install

To install the packages, please run `npm install`.

### Usage

To start the application, please run `npm run start`. The application will run on `http://localhost:3000`.

To build the application, please run `npm run build`. The output directory ist `/dist`.

### Demo

![Demo Video](https://bzh-images-bucket.s3.eu-central-1.amazonaws.com/demo.mp4)


The application is also accessable under `https://d23j0m2hdydtx3.cloudfront.net/
`
