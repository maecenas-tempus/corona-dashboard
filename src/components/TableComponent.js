import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Immutable from 'immutable';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import { fetchData, setSelecteds } from '../actions';

function descendingComparator(a, b, orderBy) {
  if (b.get(orderBy) < a.get(orderBy)) {
    return -1;
  }
  if (b.get(orderBy) > a.get(orderBy)) {
    return 1;
  }
  return 1;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  return array.sort((a, b) => comparator(a, b));
}

const headCells = [
  {
    id: 'name', numeric: false, disablePadding: true, label: 'Region',
  },
  {
    id: 'cases', numeric: true, disablePadding: false, label: 'Cases',
  },
  {
    id: 'deaths', numeric: true, disablePadding: false, label: 'Deaths',
  },
  {
    id: 'recovered', numeric: true, disablePadding: false, label: 'Recovered',
  },
];

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
  },
  th: {
    'padding-left': 0,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('region');
  const timeRange = useSelector((state) => state.get('timeRange'));
  const rows = useSelector((state) => (
    state.getIn(['data', timeRange], undefined)
  ));
  const selecteds = useSelector((state) => (
    state.get('selecteds', Immutable.List())
  ));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      if (selecteds.size !== rows.size && selecteds.size !== 0) {
        dispatch(setSelecteds(Immutable.List()));
        return;
      }
      dispatch(setSelecteds(rows.map((i) => i.get('id'))));
    } else {
      dispatch(setSelecteds(Immutable.List()));
    }
  };

  const handleClick = (event, rowId) => {
    const newSelecteds = selecteds.includes(rowId)
      ? selecteds.delete(selecteds.indexOf(rowId))
      : selecteds.push(rowId);
    dispatch(setSelecteds(newSelecteds));
  };

  if (!rows) {
    return null;
  }

  return (
    <Grid container className={classes.root} spacing={1} direction="column">
      <TableContainer>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          aria-label="enhanced table"
          size="small"
          stickyHeader
        >
          <TableHead>
            <TableRow>
              <TableCell className={classes.th}>
                <Checkbox
                  indeterminate={
                    selecteds.size > 0 && selecteds.size < rows.size
                  }
                  checked={rows.size > 0 && selecteds.size === rows.size}
                  onChange={handleSelectAllClick}
                  inputProps={{ 'aria-label': 'select all desserts' }}
                  size="small"
                  padding="none"
                />
              </TableCell>
              {headCells.map((headCell) => (
                <TableCell
                  className={classes.th}
                  key={headCell.id}
                  align={headCell.numeric ? 'right' : 'left'}
                  padding={headCell.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === headCell.id ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === headCell.id}
                    direction={orderBy === headCell.id ? order : 'asc'}
                    onClick={(event) => handleRequestSort(event, headCell.id)}
                  >
                    {headCell.label}
                    {orderBy === headCell.id ? (
                      <span className={classes.visuallyHidden}>
                        {order === 'desc'
                          ? 'sorted descending'
                          : 'sorted ascending'}
                      </span>
                    ) : null}
                  </TableSortLabel>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {stableSort(rows, getComparator(order, orderBy)).map((row) => {
              const isItemSelected = selecteds.includes(row.get('id'));
              const labelId = `enhanced-table-checkbox-${row.get('id')}`;

              return (
                <TableRow
                  hover
                  onClick={(event) => handleClick(event, row.get('id'))}
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  key={row.get('id')}
                  selected={isItemSelected}
                >
                  <TableCell className={classes.th}>
                    <Checkbox
                      checked={isItemSelected}
                      inputProps={{ 'aria-labelledby': labelId }}
                      size="small"
                    />
                  </TableCell>
                  <TableCell
                    component="th"
                    id={labelId}
                    scope="row"
                    padding="none"
                  >
                    {row.get('name')}
                  </TableCell>
                  <TableCell align="right">{row.get('cases')}</TableCell>
                  <TableCell align="right">{row.get('deaths')}</TableCell>
                  <TableCell align="right">{row.get('recovered')}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Grid>
  );
}
