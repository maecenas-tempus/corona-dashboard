function checkStatus(response) {
  if (response.ok) {
    return response;
  }
  const error = new Error(response.statusText);
  throw error;
}

export function fetchAndCheck(url, options = {}) {
  Object.assign(options, { credentials: 'same-origin' });

  return fetch(url, options)
    .catch((err) => {
      throw new Error(err.messge, err);
    })
    .then(checkStatus);
}

export function getData(url) {
  const options = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
    },
  };
  return fetchAndCheck(url, options).then((response) => response.json());
}
