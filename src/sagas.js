import {
  call,
  put,
  select,
  takeEvery,
  takeLatest,
  delay,
} from 'redux-saga/effects';
import ACTIONS, { FETCH_DATA_URL } from './constants';
import { getData } from './fetch';

const sumDataOfFourTimeRanges = (data) => (
  {
    week_4: data.reduce((acc, curr) => acc + curr),
    week_3: data.slice(7).reduce((acc, curr) => acc + curr),
    week_2: data.slice(14).reduce((acc, curr) => acc + curr),
    week_1: data.slice(21).reduce((acc, curr) => acc + curr),
  }
);

const arrangeData = (response, property) => (
  Object.entries(response.data).map((item) => ({
    [property]: sumDataOfFourTimeRanges(
      item[1].history.map((itemInner) => itemInner[property]),
    ),
    id: item[1].id - 1,
    name: item[1].name,
    region: item[0],
  })));

const groupDataByTimeRange = (data, timeRange) => (
  data.map((item) => ({
    id: item.id,
    region: item.region,
    name: item.name,
    cases: item.cases[timeRange],
    deaths: item.deaths[timeRange],
    recovered: item.recovered[timeRange],
  })));

const summerizeByProperty = (array, property) => (
  array.map((item) => item[property]).reduce((acc, curr) => acc + curr)
);

function* getArrangedData() {
  const originalCases = yield call(
    getData,
    FETCH_DATA_URL.replace('pattern', 'cases/28'),
  );
  const cases = arrangeData(originalCases, 'cases');
  const deaths = arrangeData(
    yield call(getData, FETCH_DATA_URL.replace('pattern', 'deaths/28')),
    'deaths',
  );
  const recovered = arrangeData(
    yield call(getData, FETCH_DATA_URL.replace('pattern', 'recovered/28')),
    'recovered',
  );
  const tmpData = cases.map((value, index) => (
    { ...value, ...deaths[index], ...recovered[index] }
  ));

  return {
    data: {
      week_1: groupDataByTimeRange(tmpData, 'week_1'),
      week_2: groupDataByTimeRange(tmpData, 'week_2'),
      week_3: groupDataByTimeRange(tmpData, 'week_3'),
      week_4: groupDataByTimeRange(tmpData, 'week_4'),
    },
    meta: originalCases.meta,
  };
}

export function* sagasFetchData() {
  const { data, meta } = yield call(getArrangedData);
  yield put({
    type: ACTIONS.GOT_DATA,
    payload: { data },
  });
  yield put({
    type: ACTIONS.GOT_META,
    payload: { meta },
  });
  const sum = {};
  Object.entries(data).map((item) => {
    sum[item[0]] = {
      cases: summerizeByProperty(item[1], 'cases'),
      deaths: summerizeByProperty(item[1], 'deaths'),
      recovered: summerizeByProperty(item[1], 'recovered'),
    };
    return null;
  });
  yield put({
    type: ACTIONS.SET_SUM,
    payload: { sum },
  });
  yield put({
    type: ACTIONS.FETCH_EVERY_HOUR,
  });
}

export function* fetchEveryHour() {
  yield delay(3600000);
  const probe = yield call(
    getData,
    FETCH_DATA_URL.replace('pattern', 'cases/1'),
  );
  const lastUpdate = yield select((state) => (
    state.getIn(['meta', 'lastUpdate'])
  ));
  if (probe.meta.lastUpdate !== lastUpdate) {
    yield call(sagasFetchData);
  }
  yield put({
    type: ACTIONS.FETCH_EVERY_HOUR,
  });
}

export default function* watcher() {
  yield takeEvery(ACTIONS.FETCH_DATA, sagasFetchData);
  yield takeLatest(ACTIONS.FETCH_EVERY_HOUR, fetchEveryHour);
}
