import ACTIONS from './constants';

export function fetchData() {
  return {
    type: ACTIONS.FETCH_DATA,
  };
}

export function setSelecteds(ids) {
  return {
    type: ACTIONS.SET_SELECTEDS,
    payload: {
      selecteds: ids,
    },
  };
}

export function setTimeRange(timeRange) {
  return {
    type: ACTIONS.SET_TIME_Range,
    payload: {
      timeRange,
    },
  };
}
